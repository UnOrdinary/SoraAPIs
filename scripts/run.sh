#!/bin/bash

if [[ -d "static/css" ]]; then

	rm --force static/css/bootstrap.*
else
	mkdir --parents "static/css"
fi

if [[ -d "static/js" ]]; then

	rm --force static/js/bootstrap.*
else
	mkdir --parents "static/js"
fi

index Download static/css/bootstrap.css "https://raw.githubusercontent.com/twbs/bootstrap/main/dist/css/bootstrap.css"
index Download static/css/bootstrap.css.map "https://raw.githubusercontent.com/twbs/bootstrap/main/dist/css/bootstrap.css.map"
index Download static/css/bootstrap.min.css "https://raw.githubusercontent.com/twbs/bootstrap/main/dist/css/bootstrap.min.css"
index Download static/css/bootstrap.min.css.map "https://raw.githubusercontent.com/twbs/bootstrap/main/dist/css/bootstrap.min.css.map"

index Download static/js/bootstrap.js "https://raw.githubusercontent.com/twbs/bootstrap/main/dist/js/bootstrap.js"
index Download static/js/bootstrap.js.map "https://raw.githubusercontent.com/twbs/bootstrap/main/dist/js/bootstrap.js.map"
index Download static/js/bootstrap.bundle.js "https://raw.githubusercontent.com/twbs/bootstrap/main/dist/js/bootstrap.bundle.js"
index Download static/js/bootstrap.bundle.js.map "https://raw.githubusercontent.com/twbs/bootstrap/main/dist/js/bootstrap.bundle.js.map"
index Download static/js/bootstrap.bundle.min.js "https://raw.githubusercontent.com/twbs/bootstrap/main/dist/js/bootstrap.bundle.min.js"
index Download static/js/bootstrap.bundle.min.js.map "https://raw.githubusercontent.com/twbs/bootstrap/main/dist/js/bootstrap.bundle.min.js.map"
index Download static/js/bootstrap.min.js "https://raw.githubusercontent.com/twbs/bootstrap/main/dist/js/bootstrap.min.js"
index Download static/js/bootstrap.min.js.map "https://raw.githubusercontent.com/twbs/bootstrap/main/dist/js/bootstrap.min.js.map"
