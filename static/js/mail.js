/*!
 * Mail - JS v1.0.0 (https://soraapis.eu.org)
 * Copyright 2014-2024 Light Of The Night Sky (https://lotns.eu.org)
 * Licensed under MIT (https://soralicense.eu.org)
 */

document.querySelector(".random-email").addEventListener("click", generateRandomEmail);

function generateRandomEmail() {
	var hostDomain = window.location.hostname;
	var randomString = Math.random().toString(36).substring(2, 10);
	var email = randomString + "@" + hostDomain;
	location.href = "mailto:" + email;
}
