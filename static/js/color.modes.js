/*!
 * Color.Modes - JS v1.0.0 (https://soraapis.eu.org)
 * Copyright 2014-2024 Light Of The Night Sky (https://lotns.eu.org)
 * Licensed under MIT (https://soralicense.eu.org)
 */

function detectColorScheme() {
	if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
		document.documentElement.setAttribute("data-bs-theme", "dark");
	} else {
		document.documentElement.setAttribute("data-bs-theme", "light");
	}
}

detectColorScheme();

window.matchMedia("(prefers-color-scheme: dark)").addListener(detectColorScheme);
