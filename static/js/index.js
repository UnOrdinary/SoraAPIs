/*!
 * JS v1.4.0 (https://soraapis.eu.org)
 * Copyright 2014-2024 Light Of The Night Sky (https://lotns.eu.org)
 * Licensed under MIT (https://soralicense.eu.org)
 */

/**
 * Multiple Classes
 */

document.addEventListener("DOMContentLoaded", function () {
	// Awaiter
	const awaiterElements = document.querySelectorAll(".index-awaiter");
	window.addEventListener("load", () => {
		awaiterElements.forEach(function (awaiter) {
			setTimeout(() => {
				hideElement(awaiter);
			}, 200);
		});
	});

	// Back
	const goBackButtons = document.querySelectorAll(".index-back");
	goBackButtons.forEach(function (button) {
		button.addEventListener("click", function (event) {
			event.preventDefault();
			goBackInHistory();
		});
	});

	// Clipboard
	const copyButtons = document.querySelectorAll(".index-copy");
	copyButtons.forEach(function (button) {
		button.addEventListener("click", handleClipboardClick);
	});

	function handleClipboardClick() {
		const codeBlock = this.parentElement.nextElementSibling;
		const codeText = codeBlock.querySelector("code").innerText;

		copyTextToClipboard(codeText);

		const clipboardIcon = this;
		const checkIcon = this.nextElementSibling;

		toggleVisibility(clipboardIcon);
		toggleVisibility(checkIcon);

		setTimeout(function () {
			toggleVisibility(clipboardIcon);
			toggleVisibility(checkIcon);
		}, 2000);
	}

	// Date
	const dateElements = document.querySelectorAll(".index-date");
	dateElements.forEach(displayCurrentDate);

	// MainGen
	const randomEmailElements = document.querySelectorAll(".index-mailgen");
	randomEmailElements.forEach(function (randomEmailElement) {
		if (randomEmailElement) {
			randomEmailElement.addEventListener("click", generateRandomEmail);
		}
	});

	// User Agent
	const userAgentElements = document.querySelectorAll(".index-user-agent");
	userAgentElements.forEach(displayUserAgent);

	// Utc Date
	const dateUtcElements = document.querySelectorAll(".index-utc-date");
	dateUtcElements.forEach(displayUtcDate);

	// Year
	const yearElements = document.querySelectorAll(".index-year");
	yearElements.forEach(displayCurrentYear);

	/* ----------------- Functions ----------------- */

	function hideElement(element) {
		if (element) {
			element.style.visibility = "hidden";
		}
	}

	function showElement(element) {
		if (element) {
			element.style.visibility = "visible";
		}
	}

	function toggleVisibility(element) {
		if (element) {
			element.classList.toggle("d-none");
		}
	}

	function goBackInHistory() {
		history.go(-1);
	}

	function copyTextToClipboard(text) {
		const textarea = document.createElement("textarea");
		textarea.value = text;

		document.body.appendChild(textarea);
		textarea.select();
		document.execCommand("copy");
		document.body.removeChild(textarea);
	}

	function displayCurrentDate(element) {
		if (element) {
			element.textContent = new Date();
		}
	}

	function generateRandomEmail() {
		const hostDomain = window.location.hostname;
		const randomString = Math.random().toString(36).substring(2, 10);
		const email = randomString + "@" + hostDomain;
		location.href = "mailto:" + email;
	}

	function displayUserAgent(element) {
		if (element) {
			element.textContent = navigator.userAgent;
		}
	}

	function displayUtcDate(element) {
		if (element) {
			element.textContent = new Date().toUTCString().replace("GMT", "");
		}
	}

	function displayCurrentYear(element) {
		if (element) {
			element.textContent = new Date().getFullYear();
		}
	}
});

/**
 * Redirect
 */

const redirects = new Map([
	// Light Of The Night Sky (LOTNS)
	["#lotns", "https://lotns.eu.org"],
	["#donate", "https://lotns.eu.org/donate"],
	["#legal", "https://lotns.eu.org/legal"],
	["#privacy", "https://lotns.eu.org/legal#privacy-policy"],
	["#terms", "https://lotns.eu.org/legal#terms-of-service"],
	// Start with "/" Slash need to be Same Origin Or need to have html file with same name.
	["/legal#privacy", "/legal#privacy-policy"],
	["/legal#terms", "/legal#terms-of-service"],

	// SoraAPIs
	["#apis", "https://soraapis.eu.org"],

	// SoraBlog
	["#blog", "https://sorablog.eu.org"],
	["/apache-nginx-with-hns-icann-domain", "/p/1"],
	["/adguard-home-with-handshake", "/p/0"],

	// SoraCDNs
	["#cdns", "https://soracdns.eu.org"],

	// SoraDNS
	["#dns", "https://soradns.eu.org"],

	// SoraFonts
	["#fonts", "https://sorafonts.eu.org"],

	// SoraID
	["#id", "https://soraid.eu.org"],

	// SoraLicense
	["#license", "https://soralicense.eu.org"],

	// SoraStatus
	["#status", "https://sorastatus.eu.org"],

	// UnOrdinary
	["#me", "https://unordinary.eu.org"],
]);

function handleRedirects() {
	const currentPath = window.location.pathname;
	const currentHash = window.location.hash;

	const redirect = redirects.get(currentPath + currentHash) || redirects.get(currentHash) || redirects.get(currentPath);

	if (redirect) {
		window.location.replace(redirect);
	}
}

handleRedirects();
addEventListener("hashchange", handleRedirects, false);

/**
 * ThemePref
 */

function setTheme() {
	const isDarkMode = window.matchMedia("(prefers-color-scheme: dark)").matches;
	document.documentElement.setAttribute("data-bs-theme", isDarkMode ? "dark" : "light");
}

// Initial setup
setTheme();

// Listen for changes in color scheme preference
window.matchMedia("(prefers-color-scheme: dark)").addListener(setTheme);
