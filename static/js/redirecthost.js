// redirect.js

// Mapping of old domains to their corresponding new domains
const domainRedirects = {
	"localhost:8443": "https://examplenew.org",
	"exampleold2.org": "https://examplenew.org",
	"exampleold3.org": "https://examplenew.org",
	"shitold.org": "https://shitnew.org",
	"shitold2.org": "https://shitnew.org",
	"shitold3.org": "https://shitnew.org",
};

// Get the current host
const currentHost = window.location.host;

// Check if the current host is in the mapping
if (domainRedirects[currentHost]) {
	// Construct the new URL
	const newUrl = domainRedirects[currentHost] + window.location.pathname + window.location.search;

	// Redirect to the new URL
	window.location.replace(newUrl);
}
