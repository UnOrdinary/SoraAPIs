/*!
 * Redirect - JS v1.1.0 (https://soraapis.eu.org)
 * Copyright 2014-2024 Light Of The Night Sky (https://lotns.eu.org)
 * Licensed under MIT (https://soralicense.eu.org)
 */

const redirects = new Map([
	// Light Of The Night Sky (LOTNS)
	["#lotns", "https://lotns.eu.org"],
	["#donate", "https://lotns.eu.org/donate"],
	["#legal", "https://lotns.eu.org/legal"],
	["#privacy", "https://lotns.eu.org/legal#privacy-policy"],
	["#terms", "https://lotns.eu.org/legal#terms-of-service"],
	["/legal#privacy", "/legal#privacy-policy"],
	["/legal#terms", "/legal#terms-of-service"],

	// SoraAPIs
	["#apis", "https://soraapis.eu.org"],

	// SoraBlog
	["#blog", "https://sorablog.eu.org"],
	["/apache-nginx-with-hns-icann-domain", "/p/1"],
	["/adguard-home-with-handshake", "/p/0"],
	["/p/sample", "/404"],

	// SoraCDNs
	["#cdns", "https://soracdns.eu.org"],

	// SoraDNS
	["#dns", "https://soradns.eu.org"],

	// SoraFonts
	["#fonts", "https://sorafonts.eu.org"],

	// SoraLicense
	["#license", "https://soralicense.eu.org"],

	// SoraStatus
	["#status", "https://sorastatus.eu.org"],

	// UnOrdinary
	["#me", "https://unordinary.eu.org"],
]);

function handleHash() {
	const currentPath = window.location.pathname;
	const currentHash = window.location.hash;

	const redirect = redirects.get(currentPath + currentHash) || redirects.get(currentHash) || redirects.get(currentPath);

	if (redirect) {
		window.location.replace(redirect);
	}
}

handleHash();
addEventListener("hashchange", handleHash, false);
