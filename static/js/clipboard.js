/*!
 * Clipboard - JS v1.2.0 (https://soraapis.eu.org)
 * Copyright 2014-2024 Light Of The Night Sky (https://lotns.eu.org)
 * Licensed under MIT (https://soralicense.eu.org)
 */

document.addEventListener("DOMContentLoaded", function () {
	var copyButtons = document.querySelectorAll(".copy-button");

	copyButtons.forEach(function (button) {
		button.addEventListener("click", function () {
			var codeBlock = this.parentElement.nextElementSibling;
			var codeText = codeBlock.querySelector("code").innerText;

			var textarea = document.createElement("textarea");
			textarea.value = codeText;

			document.body.appendChild(textarea);
			textarea.select();
			document.execCommand("copy");
			document.body.removeChild(textarea);

			var clipboardIcon = this;
			var checkIcon = this.nextElementSibling;

			clipboardIcon.classList.add("d-none");
			checkIcon.classList.remove("d-none");

			setTimeout(function () {
				clipboardIcon.classList.remove("d-none");
				checkIcon.classList.add("d-none");
			}, 2000);
		});
	});
});
