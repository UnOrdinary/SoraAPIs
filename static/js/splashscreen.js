/*!
 * SplashScreen - JS v1.1.0 (https://soraapis.eu.org)
 * Copyright 2014-2024 Light Of The Night Sky (https://lotns.eu.org)
 * Licensed under MIT (https://soralicense.eu.org)
 */

document.addEventListener("DOMContentLoaded", function () {
	const SplashScreen = document.querySelector(".splash-screen");
	window.addEventListener("load", () => {
		setTimeout(() => {
			SplashScreen.style.display = "none";
		}, 200);
	});
});
